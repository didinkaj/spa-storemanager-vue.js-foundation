const profile = {
  'firstName': 'Didinya',
  'lastName': 'Johnson',
  'limit': 20000
}

const products = [
  { 'id': 1, 'title': 'Sugar 1kg', 'price': 100, 'inventory': 2, 'shipping': 10 },
  { 'id': 2, 'title': 'Bread 500g', 'price': 80, 'inventory': 10, 'shipping': 10 },
  { 'id': 3, 'title': 'Rice 1kg', 'price': 130, 'inventory': 3, 'shipping': 10 },
  { 'id': 4, 'title': 'Maize flour 2kg', 'price': 90, 'inventory': 5, 'shipping': 20 },
  { 'id': 5, 'title': 'Wheat flour 2kg', 'price': 120, 'inventory': 1, 'shipping': 20 },
  { 'id': 6, 'title': 'Soap 1 bar', 'price': 130, 'inventory': 6, 'shipping': 10 }
]

const promotions = [
  { 'id': 1, 'title': '30% OFF' },
  { 'id': 2, 'title': '$100.00 Discount' },
  { 'id': 3, 'title': 'Free Shipping' },
  { 'id': 4, 'title': '+ $100.00 on limit' }
]

// Simulate requests

export default {
  getProfile (cb) {
    setTimeout(() => cb(profile), 200)
  },

  getProducts (cb) {
    setTimeout(() => cb(products), 200)
  },

  getPromotions (cb) {
    setTimeout(() => cb(promotions), 200)
  }
}
